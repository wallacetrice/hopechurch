<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> welcome to Delight Restaurant</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="main-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<!--header-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 top-header">
            <div class="row">
                <div class="col-md-2 col-md-offset-1 sub-top-header">
                    <img src="images/Capture.PNG" height="150">
                </div>
                <div class="col-md-8">
                    <div class="header-top">
                        <h1><b>HOPE TABERNACLE MINISTRIES</b</h1>
                        <h4 style="color: red; font-size: 20px; padding-left: 20px"><i><b>Restoring hopes </b></i></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- toggle icon-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a class="nav-link" href="index.php">HOME</a></li>
                        <li><a class="nav-link" href="about.php">ABOUT US</a></li>
                        <li><a class="nav-link" href="programs.php">CHURCH PROGRAMS</a></li>
                        <li><a class="nav-link" href="contact.php">CONTACT US</a></li>
                    </ul>
                    <form class="navbar-form navbar-right" id="frmSearch">
                        <div class="form-group">
                            <input id="txtSearch" type="text" class="form-control" placeholder="Search here...">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
</div>

<!--main body-->
<div class="container-fluid">
    <div class="row">
        <div class="home-image text-center">
            <h1 class="home-title">WELCOME TO HOPE TABERNACLE MINISTRIES <span class="home-sub-title"></span></h1>
        </div>
    </div>
</div>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-4">
            <div class="sectionOne"><br>
                <img src="images/image3.jpg" alt="" width="300" height="350">
            </div>
            <br>
        </div><br>
        <div class="col-md-8">
            <div class="sectionOne">
                <h2>----WELCOME MESSAGE----</h2>
                <p><b>Welcome to Hope  Tabernacle Ministries-</b> a christian ministry birthed in Gulu Uganda in 2016. We have planted
                    Spirit filled Evangelical churches in Lira, Kitgum, Kampala, and Hoima reaching out to different parts of uganda
                    with love of Jesus Christ</p>
                <p>Hope Tabernacle Ministries continues to experience the outpouring of God's richest blessings throughout the many areas
                    of involvement with in our communities.</p>
            </div>
        </div>
    </div><br>
</div>
<!--footer-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="background-color: deepskyblue; height: 60px">
            <div class="footer">
                <p>  <b>Follow us on </b>  <a href="#" class="fa fa-facebook"></a>    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-whatsapp"></a>   </p> <br>
            </div>
        </div>
        <div class="col-md-12" style="background-color: deepskyblue; height: 60px">
            <div class="footer">
                <p> <b>  &copy; Copyright 2018 - Hope Tabernacle Ministries.  All rights reserved.</b></p>
            </div>
        </div>
    </div>
</div>

<!--javascript plugins-->
<!--search bar working-->
<script type="text/javascript">
    document.getElementById('frmSearch').onsubmit = function() {
        window.location = 'http://www.google.com/search?q=site:yoursitename.com ' + document.getElementById('txtSearch').value;
        return false;
    }
</script>

<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html