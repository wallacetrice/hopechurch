<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> welcome to Delight Restaurant</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="main-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<!--header-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 top-header">

            <div class="row">
                <div class="col-md-2 col-md-offset-1 sub-top-header">
                    <img src="images/Capture.PNG" height="150">
                </div>
                <div class="col-md-8">
                    <div class="header-top">
                        <h1><b>HOPE TABERNACLE MINISTRIES</b</h1>
                        <h4 style="color: red; font-size: 20px; padding-left: 20px"><i><b>Restoring hopes </b></i></h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- toggle icon-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a class="nav-link" href="index.php">HOME</a></li>
                        <li><a class="nav-link" href="about.php">ABOUT US</a></li>
                        <li><a class="nav-link" href="programs.php">CHURCH PROGRAMS</a></li>
                        <li><a class="nav-link" href="contact.php">CONTACT US</a></li>
                    </ul>
                    <form class="navbar-form navbar-right" id="frmSearch">
                        <div class="form-group">
                            <input id="txtSearch" type="text" class="form-control" placeholder="Search here...">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
</div>



<!--main body-->
<div class="container-fluid">
    <div class="row">
        <div class="mini-header text-center">
            <h1 class="headerTitle">CHURCH PROGRAMS <span class="sub-mini-title"></span></h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="sectionSeven"><br>
                <table class="table">
                    <h3>CHURCH PROGRAMS</h3>
                    <thead>
                    <tr class="bg-primary">
                        <th>DAYS</th>
                        <th>SERVICES</th>
                        <th>TIME</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="bg-success">
                        <td>Monday</td>
                        <td>Bible Study</td>
                        <td>5:30pm - 7:30pm</td>
                    </tr>
                    <tr class="alert-danger">
                        <td>Tuesday</td>
                        <td>Home Fellowships</td>
                        <td>5:30pm - 7:30pm</td>
                    </tr>
                    <tr class="bg-success">
                        <td>Wednesday</td>
                        <td>Mid-Week service</td>
                        <td>5:30pm - 7:30pm</td>
                    </tr>
                    <tr class="alert-danger">
                        <td>Thursday</td>
                        <td>Choir Practise and Counseling</td>
                        <td>5:30pm - 7:30pm</td>
                    </tr>
                    <tr class="bg-success">
                        <td>Friday</td>
                        <td>Overnight</td>
                        <td>8:00pm - 6:00am</td>
                    </tr>
                    <tr class="alert-danger">
                        <td>Saturday</td>
                        <td>Choir Practise and Church cleaning</td>
                        <td>2:00pm - 6:00pm</td>
                    </tr>
                    <tr class="bg-success">
                        <td>Sunday</td>
                        <td>English Service</td>
                        <td>8:00am - 10:00am</td>
                    </tr>
                    <tr class="bg-success">
                        <td></td>
                        <td>Translated Service from English to Luo</td>
                        <td>10:30am - 1:00pm</td>
                    </tr>
                    </tbody>
                </table><br>
                <h4 style="color: red">NOTE</h4>
                <p>On Tuesday, Home Fellowships service time varies depending on the time the family members are all home
                 however they also start from 5:30pm - 7:30pm</p>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-1">
            <div class="sectionSeven"><br>
                <h3>TESTIMONY FORM</h3>
                <p>Do you have an inspiring testimony you want to share with Hope Tabernacle Ministries? Please fill the form below
                and send us your testimony</p>
                <br>
                <form id="contact-form" name="contact-form" action="connect%20_testimony.php" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fName" class="">First Name:</label>
                                <input type="text" id="fName" name="fName" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lName" class="">Last Name:</label>
                                <input type="text" id="lName" name="lName" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email" class="">Email:</label>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="testimony">Testimony:</label>
                                <textarea id="testimony" name="testimony" rows="4" class="form-control md-textarea" placeholder="write here your testimony" required></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="form-button" style="text-align: right">
                        <button type="reset" class="btn btn-primary" value="reset">Clear</button>
                        <button type="submit" class="btn btn-primary" value="submit">Send</button>
                    </div>
                </form>
            </div>
        </div>
        <br>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="sectionSeven">
                <h3>ANNUAL YOUTH CONFERENCE</h3>
                <img src="images/image11.jpg" alt="" width="1000" height="400">
            </div>
        </div>
    </div>
</div>
<br>
<!--footer-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="background-color: deepskyblue; height: 60px">
            <div class="footer">
                <p>  <b>Follow us on </b>  <a href="#" class="fa fa-facebook"></a>    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-whatsapp"></a>   </p> <br>
            </div>
        </div>
        <div class="col-md-12" style="background-color: deepskyblue; height: 60px">
            <div class="footer">
                <p> <b>  &copy; Copyright 2018 - Hope Tabernacle Ministries.  All rights reserved.</b></p>
            </div>
        </div>
    </div>
</div>

<!--javascript plugins-->
<!--search bar working-->
<script type="text/javascript">
    document.getElementById('frmSearch').onsubmit = function() {
        window.location = 'http://www.google.com/search?q=site:yoursitename.com ' + document.getElementById('txtSearch').value;
        return false;
    }
</script>

<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html